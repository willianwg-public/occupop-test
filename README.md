# Application Documentation

## Introduction

This application is a simple authentication system that allows users to log in using their email and password credentials. It contains pre-defined user accounts with corresponding email and password combinations.

## Pre-defined Users

The application includes the following pre-defined user accounts:

1. **User 1**
   - Email: user.test@test.com
   - Password: occupoptest

2. **User 2**
   - Email: occupop@company.com
   - Password: occupopcompanypassword123

3. **User 3**
   - Email: paul@occupop.com
   - Password: Paulpas$wordoccupop01

## Authentication Route

Users can log in to the application using the `/login` route via a POST request. They should provide their email and password as parameters in the request body.

### Request Example:
To login, send a POST request to `http://localhost:3001/login` with the following JSON payload:
```json
{
	"email":"user.test@test.com",
	"password":"occupoptest"
}
```

### Successful Response Example:
```json
{
	"accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJwYXVsQG9jY3Vwb3AuY29tIiwiaWF0IjoxNzA4NDY0MzM2LCJleHAiOjE3MDg0NzUxXzF9.yTKTSysZn66QA_YIYyq7B1awcpyzMYSWqzysC1iJRy4",
	"user": {
		"id": 1,
		"email": "user.test@test.com"
	}
}
```

## End-to-End Tests

The application includes end-to-end (e2e) tests for the authentication route. These tests can be found in the `/test` directory.
You can run the tests using the command `npm run test:e2e`

## Database Choice

MySQL was chosen as the database for this application due to the developer's familiarity and experience with it. While MongoDB was also an option, MySQL was selected for its relational database capabilities and the developer's confidence in working with it.

## Running the Project

### Without Docker

1. Make sure you have Node.js and npm installed on your machine.
2. Navigate to the project directory in your terminal.
3. Run `npm install` to install the dependencies.
4. Modify the values in the `.env` file to point to your existing and running MySQL database.
5. Run `npm run start:dev` to start the application in development mode.
6. Access the application at `http://localhost:3001` after it has successfully started.

### With Docker Compose

1. Make sure you have Docker and Docker Compose installed on your machine.
2. Navigate to the project directory in your terminal.
3. Run `docker-compose up` to build and start the application containers.

**Note**: If the application fails to start due to the database not being ready, please wait for the database to become available before retrying. The application logs will cease displaying errors once the database is ready.

4. Access the application at `http://localhost:3001` after it has successfully started.