import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  const USER_EMAIL_TEST = 'user.test@test.com';
  const USER_PASSWORD_TEST = 'occupoptest';

  const LOGIN_ROUTE = '/login';

  it('Should login successfully', async () => {
    const response = await request(app.getHttpServer()).post(LOGIN_ROUTE).send({
      email: USER_EMAIL_TEST,
      password: USER_PASSWORD_TEST,
    });

    expect(response.status).toEqual(200);
    expect(response.body).toHaveProperty('accessToken');
  });
  it('Should get error 401 when trying to login using wrong password', async () => {
    const response = await request(app.getHttpServer()).post(LOGIN_ROUTE).send({
      email: USER_EMAIL_TEST,
      password: 'wrongPassowrd',
    });

    expect(response.status).toEqual(401);
  });
  it('Should get error 404 when trying to login using not existing email', async () => {
    const response = await request(app.getHttpServer()).post(LOGIN_ROUTE).send({
      email: 'wrongEmail',
      password: USER_PASSWORD_TEST,
    });

    expect(response.status).toEqual(404);
  });
});
