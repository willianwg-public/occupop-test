import { MigrationInterface, QueryRunner } from 'typeorm';

export class PopulateUsers1708461985035 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO user (email, password) VALUES 
        ("paul@occupop.com", "$2b$10$4fzAxIihtl0IskSxEdxlnumSR373VHK91CkEdxqSH01qN67u8unC6"),
        ("occupop@company.com", "$2b$10$0xeAlkbf0GRRbo9JuFq8EuJ8VFx8eK.QTbaP1E.7JovjQp1DRHBe2"),
        ("user.test@test.com", "$2b$10$rKwJSNmGBAXmYt4CdFMsSe0l1bN6usHUAxwQrDPxXZXKd.iBvmOuy");
      `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM user`);
  }
}
