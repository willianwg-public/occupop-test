import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { LoginDTO } from './dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @HttpCode(200)
  @Post('login')
  async login(@Body() body: LoginDTO) {
    return this.appService.login(body);
  }
}
